import {createStore, clear, del, get, keys, set} from "idb-keyval";

const storeName = "TaskStore";
const log = (...params) => console.log(`${storeName}:`, ...params);

const fileStore = createStore("task-files", "task-files-store");

export function FS () {
    const getFilenames = () => {
        return keys(fileStore);
    }
    const getFile = (filename) => {
        return get(filename, fileStore);
    }
    const setFile = (filename, data) => {
        return set(filename, data, fileStore);
    }
    const delFile = (filename) => {
        return del(filename, fileStore);
    }
    const reset = () => {
        return clear(fileStore);
    }

    const ready = getFilenames()
        .then((filenames) => {
            if (filenames.length === 0) {
                // database is empty, reset to insert example files.
                return reset();
            }
        });

    return {
        getFilenames,
        getFile,
        setFile,
        delFile,
        reset,
        ready
    };
}
