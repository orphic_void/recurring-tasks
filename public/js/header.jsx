import Header from "rsuite/Header";
import Navbar from "rsuite/Navbar";
import Nav from "rsuite/Nav";
import React from "react";

export default function Header(props) {
    const newItem = props.edit;

    return (<Header>
        <Navbar>
            <Navbar.Brand>
                Task Tracker
            </Navbar.Brand>
            <Nav>
                <Nav.Item>🏠 Home</Nav.Item>
                <Nav.Item onClick={newItem}>New Item</Nav.Item>
            </Nav>
            <Nav pullRight>
                <Nav.Item>⚙️ Settings</Nav.Item>
            </Nav>
        </Navbar>
    </Header>);
}
