import Table from "rsuite/Table";
import {Icon} from "@rsuite/icons";
import Progress from "rsuite/Progress";
import React from "react";

const data =
    [
        {
            "id": "1",
            "labelName": "Car",
            "status": "ENABLED",
            "children": [
                {
                    "id": "1-1",
                    "labelName": "Mercedes Benz",
                    "status": "ENABLED",
                    "count": 460
                },
                {
                    "id": "1-2",
                    "labelName": "BMW",
                    "status": "ENABLED",
                    "children": [
                        {
                            "id": "1-2-1",
                            "labelName": "2 series",
                            "status": "ENABLED",
                            "count": 103,
                            "children": [
                                {
                                    "id": "1-2-1-1",
                                    "labelName": "Sporty hatchback",
                                    "status": "DISABLED",
                                    "count": 502
                                },
                                {
                                    "id": "1-2-1-2",
                                    "labelName": "Coupe",
                                    "status": "ENABLED",
                                    "count": 502
                                },
                                {
                                    "id": "1-2-1-3",
                                    "labelName": "Roadster",
                                    "status": "DISABLED"
                                },
                                {
                                    "id": "1-2-1-4",
                                    "labelName": "Multi-function wagon",
                                    "status": "DISABLED"
                                },
                                {
                                    "id": "1-2-1-5",
                                    "labelName": "Station wagon",
                                    "status": "DISABLED",
                                    "count": 34
                                }
                            ]
                        },
                        {
                            "id": "1-2-2",
                            "labelName": "The intention of customers",
                            "status": "ENABLED",
                            "count": 364,
                            "children": [
                                {
                                    "id": "1-2-2-1",
                                    "labelName": "Financial plan",
                                    "status": "DISABLED"
                                },
                                {
                                    "id": "1-2-2-2",
                                    "labelName": "Appointment test drive",
                                    "status": "ENABLED"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": "2",
            "labelName": "Game",
            "status": "ENABLED",
            "count": 834,
            "children": [
                {
                    "id": "2-1",
                    "labelName": "Online game",
                    "status": "DISABLED"
                },
                {
                    "id": "2-2",
                    "labelName": "Mobile game",
                    "status": "ENABLED"
                }
            ]
        },
        {
            "id": "3",
            "labelName": "Digital",
            "status": "ENABLED",
            "count": 534,
            "children": [
                {
                    "id": "3-1",
                    "labelName": "Mobile phone",
                    "status": "ENABLED",
                    "children": []
                },
                {
                    "id": "3-2",
                    "labelName": "Computer",
                    "status": "DISABLED"
                },
                {
                    "id": "3-3",
                    "labelName": "Watch",
                    "status": "ENABLED"
                }
            ]
        }
    ];

const getMax = (array) => {
    return array.reduce((a, b) => {
        let max = a;

        if (b.count) {
            max = Math.max(max, b.count);
        }
        if (b.children) {
            max = Math.max(max, getMax(b.children));
        }

        return max;
    }, 0);
};

const max = getMax(data);

export default function Table() {
    return (<Table
        isTree
        defaultExpandAllRows
        rowKey="id"
        autoHeight={true}
        data={data}
        onExpandChange={(isOpen, rowData) => {
            console.log(isOpen, rowData);
        }}
        renderTreeToggle={(icon, rowData) => {
            if (rowData.children && rowData.children.length === 0) {
                return <Icon icon="spinner" spin/>;
            }
            return icon;
        }}
    >
        <Table.Column flexGrow={1}>
            <Table.HeaderCell>Label</Table.HeaderCell>
            <Table.Cell dataKey="labelName"/>
        </Table.Column>

        <Table.Column width={100}>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.Cell dataKey="status"/>
        </Table.Column>

        <Table.Column width={100}>
            <Table.HeaderCell>Count</Table.HeaderCell>
            <Table.Cell dataKey="count"/>
        </Table.Column>

        <Table.Column flexGrow={2}>
            <Table.HeaderCell/>
            <Table.Cell>
                {rowData =>
                    (rowData.count ? <Progress percent={((rowData.count || 0) / max) * 100} showInfo={false}/> : undefined)
                }
            </Table.Cell>
        </Table.Column>
    </Table>);
}
