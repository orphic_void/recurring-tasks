import React, {useState} from "react";

import Col from "rsuite/Col";
import Container from "rsuite/Container";
import Content from "rsuite/Content";
import Header from "./header";
import Grid from "rsuite/Grid";
import Table from "./table";
import Task from "./task";

// import default style
import "rsuite/styles/index.less";


export default function App () {
    const [edit, setEdit] = useState(false);

    const toggleEdit = () => {
        setEdit((last) => !last);
    };

    return (
        <Container>
            <Header edit={toggleEdit}/>
            <Content>
                <Grid fluid style={{height: "100%"}}>
                    <Col xs={24} md={(24 / (edit ? 1.2 : 1))}><Table/></Col>
                    {edit && (<Col xs={24} md={4}><Task close={toggleEdit}/></Col>)}
                </Grid>
            </Content>
        </Container>
    );
}
