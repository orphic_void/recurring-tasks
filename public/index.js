import React from "react";
import ReactDOM from "react-dom";
import App from "./js/app.jsx";

ReactDOM.render(React.createElement(App), document.getElementById("app"));
